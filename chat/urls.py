from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^chat/', include('chatapp.urls')),
    url(r'^admin/', include(admin.site.urls)),
)