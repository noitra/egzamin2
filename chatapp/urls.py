from django.conf.urls import patterns, url
import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^login$', views.loginForm, name='loginForm'),
    url(r'^dodajWiadosc$', views.dodajwiadomosc, name='chatuj'),
    url(r'^wiadomosci$', views.messages, name='msgs'),
    url(r'^zaloguj$', views.login, name='login'),
    url(r'^wyloguj$', views.logout, name='logout'),
)