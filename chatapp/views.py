# -*- coding: utf-8 -*-
# Create your views here.
from django.utils import timezone
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from models import Wiadomosc
from django.shortcuts import render
from couchdb import *

users = {'admin':True}

def index(request):
    if not request.session.__contains__('logged'):
        request.session['logged'] = False
        request.session['user'] = u"Gość"
    registerapp()
    username = request.session['user']
    log = request.session['logged']
    logusers = []
    for u in users.keys():
        if users[u]:
            logusers.append(u)
    return render(request, 'chatapp/index.html', {'username':username, 'logged':log, 'user_list': logusers})


def messages(request):
    latest_tweet_list = Wiadomosc.objects.all().order_by('pub_date')
    context = {'latest_tweet_list': latest_tweet_list}
    return render(request, 'chatapp/messages.html', context)


def loginForm(request):
    username = request.session['user']
    log = request.session['logged']
    logusers = []
    for u in users.keys():
        if users[u]:
            logusers.append(u)
    return render(request, 'chatapp/login.html', {'username':username, 'logged':log, 'user_list': logusers})


def dodajwiadomosc(request):
    user = request.session['user']
    message = request.POST['message']
    wiadomosc = Wiadomosc(user_name=user, message=message, pub_date=timezone.now())
    wiadomosc.save()
    return HttpResponseRedirect(reverse('index'))


def login(request):
    username = request.session['user']
    log = request.session['logged']
    uzytkownik = request.POST['user']
    if not request.session['logged']:
        if users.has_key(uzytkownik):
            if users[uzytkownik]:
                info = u"Dany użytkownik jest już zalogowany!"
                logusers = []
                for u in users.keys():
                    if users[u]:
                        logusers.append(u)
                return render(request, 'chatapp/login.html', {'info': info, 'username': username, 'logged': log, 'user_list': logusers})
            else:
                users[uzytkownik] = True
                request.session['user'] = uzytkownik
                request.session['logged'] = True
                username = uzytkownik
                log = True
                info = u"Zalogowałeś się jako " + uzytkownik
                logusers = []
                for u in users.keys():
                    if users[u]:
                        logusers.append(u)
                return render(request, 'chatapp/login.html', {'info': info, 'username': username, 'logged': log, 'user_list': logusers})
        else:
            users[uzytkownik] = True
            request.session['user'] = uzytkownik
            request.session['logged'] = True
            username = uzytkownik
            log = True
            info = u"Zalogowałeś się jako " + uzytkownik
            logusers = []
            for u in users.keys():
                if users[u]:
                    logusers.append(u)
            return render(request, 'chatapp/login.html', {'info': info, 'username': username, 'logged': log, 'user_list': logusers})
    else:
        username = request.session['user']
        log = request.session['logged']
        info = u"Jesteś już zalogowany jako " + request.session['user']
        logusers = []
        for u in users.keys():
            if users[u]:
                logusers.append(u)
        return render(request, 'chatapp/login.html', {'info': info, 'username': username, 'logged': log, 'user_list': logusers})


def logout(request):
    if request.session['logged']:
        users[request.session['user']] = False
        request.session['logged'] = False
        request.session['user'] = u"Gość"
        username = request.session['user']
        log = request.session['logged']
        info = u"Wylogowałeś się"
        logusers = []
        for u in users.keys():
            if users[u]:
                logusers.append(u)
        return render(request, 'chatapp/login.html', {'info': info, 'username': username, 'logged': log, 'user_list': logusers})


def registerapp():
    s = Server('http://194.29.175.241:5984')
    db = s['chat1']
    host = 'http://nameless-headland-6292.herokuapp.com'
    active = True
    delivery = 'chat/dodajWiadosc'
    try:
        db['noitra'] = {'host':host, 'active':active, 'delivery':delivery}
    except:
        pass