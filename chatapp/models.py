from django.db import models

# Create your models here.

class Wiadomosc(models.Model):
    user_name = models.CharField(max_length=50)
    message = models.CharField(max_length=500)
    pub_date = models.DateTimeField('date published')
    def __unicode__(self):
        return self.message