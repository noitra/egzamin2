from django.contrib import admin
from chatapp.models import Wiadomosc

class MSGAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['message']}),
        ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
    ]
    list_display = ('user_name', 'message', 'pub_date')
    list_filter = ['pub_date']
    search_fields = ['message']
    date_hierarchy = 'pub_date'

admin.site.register(Wiadomosc, MSGAdmin)